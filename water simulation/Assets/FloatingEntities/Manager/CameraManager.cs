﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {

	// Use this for initialization
	public Transform player;
	public Vector3 offset;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		SetToBoat ();
	}

	private void SetToBoat(){
		transform.position = player.position;
		Vector3 lookPosition = player.position;
		transform.LookAt (lookPosition);
		transform.position = transform.TransformPoint (offset);

	}
}
