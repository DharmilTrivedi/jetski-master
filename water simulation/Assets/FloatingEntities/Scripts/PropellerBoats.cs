﻿using UnityEngine;
using UnityEditor;

public class PropellerBoats : MonoBehaviour
{
	public Transform[] propellers;
	public Transform[] rudder;
	public Transform RotationForcePoint;
	private Rigidbody rb;

	public float engine_rpm { get; private set; }

	float throttle;
	int direction = 1;

	public float propellers_constant = 0.6F;
	public float engine_max_rpm = 6000.0F;
	public float acceleration_cst = 100.0F;
	public float drag = 0.01F;
	public float rotationSpeed;
	public float rotationEngine_rpm;

	private Vector3 eularRotation;
	float angle;

	void Awake ()
	{
		engine_rpm = 0F;
		throttle = 0F;
		rb = GetComponent<Rigidbody> ();
		//		angle = 0;
		print (angle);
	}

	void Update ()
	{
		float frame_rpm = engine_rpm * Time.deltaTime;
		for (int i = 0; i < propellers.Length; i++) {
			//propellers [i].localRotation = Quaternion.Euler (propellers [i].localRotation.eulerAngles + new Vector3 (0, 0, -frame_rpm));
			//			rb.AddForceAtPosition (Quaternion.Euler (0, angle, 0) * propellers [i].forward * propellers_constant * engine_rpm, propellers [i].position);
			//			rb.AddForceAtPosition (Quaternion.Euler (0, angle, 0) * propellers [i].forward * propellers_constant * engine_rpm, propellers [i].position);
			//rb.AddForceAtPosition (transform.forward * propellers_constant * engine_rpm, transform.position);
			rb.AddForce (transform.forward * engine_rpm * propellers_constant * 5);
			rb.AddForceAtPosition (RotationForcePoint.right * propellers_constant * rotationEngine_rpm * 2, RotationForcePoint.position);
		}

		eularRotation = transform.eulerAngles;
		eularRotation.z = 0;
		transform.eulerAngles = eularRotation;

		throttle *= (1.0F - drag * 0.001F);
		engine_rpm = throttle * engine_max_rpm;

		rotationSpeed *= (1.0F - drag * 0.001F);
		rotationEngine_rpm = rotationSpeed * engine_max_rpm;

		//angle = Mathf.Lerp (angle, 0.0F, 0.02F);
		//for (int i = 0; i < rudder.Length; i++)
		//	rudder [i].localRotation = Quaternion.Euler (0, angle, 0);
	}

	public void ThrottleUp ()
	{
		throttle += acceleration_cst;
		if (throttle > 2)
			throttle = 2;
	}

	public void ThrottleDown ()
	{
		throttle -= acceleration_cst;
		if (throttle < 0)
			throttle = 0;
	}

	public void Brake ()
	{
		throttle *= 0.9F;
	}

	public void Reverse ()
	{
		direction *= -1;
	}

	public void RudderRight ()
	{
		angle -= 2F;
		angle = Mathf.Clamp (angle, -90F, 90F);
		//		transform.eulerAngles = new Vector3 (0, angle, 0);
		rotationSpeed -= acceleration_cst;
		if (rotationSpeed < 1)
			rotationSpeed = -1;
		//rb.AddForceAtPosition (transform.right * 20000000, transform.position);
	}

	public void RudderDown ()
	{
		rotationSpeed = Mathf.MoveTowards (rotationSpeed, 0, 2);
	}

	public void RudderLeft ()
	{
		angle += 2F;
		angle = Mathf.Clamp (angle, -90F, 90F);
		//		transform.eulerAngles = new Vector3 (0, angle, 0);
		rotationSpeed += acceleration_cst;
		if (rotationSpeed > 1)
			rotationSpeed = 1;
		//		rb.AddForce()
		print (rotationSpeed);

	}

	void OnDrawGizmos ()
	{
		Handles.Label (propellers [0].position, engine_rpm.ToString ());
	}
}
